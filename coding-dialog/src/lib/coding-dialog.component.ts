import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogModule } from 'primeng/dialog';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { Coding } from '@his-viewmodel/double-list';
import { InputTextModule } from 'primeng/inputtext';

@Component({
  selector: 'lib-coding-dialog',
  standalone: true,
  imports: [
    CommonModule,
    DialogModule,
    TableModule,
    ButtonModule,
    InputTextModule,
  ],
  templateUrl: './coding-dialog.component.html',
  styleUrls: ['./coding-dialog.component.scss'],
})
export class CodingDialogComponent {

  @Input() value!: Coding[];
  @Input() coding!: Coding;
  @Input() title!: string;
  @Input() visible!: boolean;

  @Output() changeVisible = new EventEmitter<boolean>();
  @Output() codingChange = new EventEmitter();

  #selectCoding!: Coding;

  onDoubleClick(coding: Coding) {
    this.codingChange.emit(coding);
    this.changeVisible.emit(false);
  }

  onCloseDialog() {
    this.visible = false;
    this.changeVisible.emit(this.visible);
  }

  onSelectCoding(coding: Coding) {
    this.#selectCoding = coding;
  }

  onOkClick() {
    this.codingChange.emit(this.#selectCoding);
    this.changeVisible.emit(false);
  }
}
